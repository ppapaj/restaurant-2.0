package pk.firafalo_pipapaj.restaurant.tests;

import org.junit.Test;
import static org.junit.Assert.*;
import static pk.firafalo_pipapaj.restaurant.tests.TestUtil.employeeNotEmpty;

import pk.firafalo_pipapaj.restaurant.DataFactory;
import pk.firafalo_pipapaj.restaurant.models.Cook;
import pk.firafalo_pipapaj.restaurant.models.Waiter;

import java.util.List;

public class DataFactoryTest {
    @Test
    public void shouldCreateValidCookList() {
        List<Cook> list = DataFactory.createCookList();

        assertFalse(list.isEmpty());

        list.forEach(TestUtil::employeeNotEmpty);
    }

    @Test
    public void shouldCreateValidWaiterList() {
        List<Waiter> list = DataFactory.createWaiterList();

        assertFalse(list.isEmpty());

        for(Waiter w: list) {
            employeeNotEmpty(w);
            assertNotEquals(0.0, w.getAppearance());
        }
    }
}
