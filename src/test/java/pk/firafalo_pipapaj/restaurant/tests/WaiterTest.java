package pk.firafalo_pipapaj.restaurant.tests;

import com.google.gson.Gson;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static pk.firafalo_pipapaj.restaurant.tests.TestUtil.DELTA;

import pk.firafalo_pipapaj.restaurant.models.Waiter;

public class WaiterTest {

    private static final String testJson = "{ \"name\": \"Toni Sanchez\", \"imageFilename\": \"toni_sanchez.jpg\", \"level\": 1, \"salary\": 50.0, \"skill\": 0.5, \"appearance\": 0.3 }";

    private static Gson gson;
    private static Waiter testWaiter;

    @BeforeClass
    public static void beforeClass() {
        gson = new Gson();
    }

    @Before
    public void beforeEachTest() {
        testWaiter = gson.fromJson(testJson, Waiter.class);
    }

    @Test
    public void shouldImportFromJson() {
        Waiter w = gson.fromJson(testJson, Waiter.class);

        assertEquals("Toni Sanchez", w.getName());
        assertEquals("toni_sanchez.jpg", w.getImageFilename());
        assertEquals(1, w.getLevel());
        assertEquals(50.0, w.getSalary(), DELTA);
        assertEquals(0.5, w.getSkill(), DELTA);
        assertEquals(0.3, w.getAppearance(), DELTA);
    }

    @Test
    public void shouldIncreaseSalaryOnLevelUp() {
        float salary = testWaiter.getSalary();
        testWaiter.levelUp();

        assertTrue("Salary greater after lvl up", salary < testWaiter.getSalary());
    }
}
