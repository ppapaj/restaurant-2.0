package pk.firafalo_pipapaj.restaurant.tests;

import pk.firafalo_pipapaj.restaurant.models.Employee;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TestUtil {
    public static final double DELTA = 1e-5;

    public static void employeeNotEmpty(Employee e) {
        assertNotEquals("", e.getName());
        assertNotEquals("", e.getImageFilename());
        assertEquals(0, e.getExperience());
        assertNotEquals(0, e.getLevel());
        assertNotEquals(0.0, e.getSalary(), DELTA);
        assertNotEquals(0.0, e.getSkill(), DELTA);
    }
}
