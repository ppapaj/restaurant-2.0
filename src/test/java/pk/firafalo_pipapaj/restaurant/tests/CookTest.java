package pk.firafalo_pipapaj.restaurant.tests;

import com.google.gson.Gson;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static pk.firafalo_pipapaj.restaurant.tests.TestUtil.DELTA;

import pk.firafalo_pipapaj.restaurant.models.Cook;

public class CookTest {

    private static final String testJson = "{\"name\": \"Nick Andrews\", \"imageFilename\": \"nick_andrews.jpg\", \"level\": 5, \"salary\": 150.0, \"skill\": 3.54}";

    private static Gson gson;
    private static Cook testCook;

    @BeforeClass
    public static void beforeClass() {
        gson = new Gson();
    }

    @Before
    public void beforeEachTest() {
        testCook = gson.fromJson(testJson, Cook.class);
    }

    @Test
    public void shouldImportFromJson() {

        Cook c = gson.fromJson(testJson, Cook.class);

        assertEquals("Nick Andrews", c.getName());
        assertEquals("nick_andrews.jpg", c.getImageFilename());
        assertEquals(5, c.getLevel());
        assertEquals(150.0, c.getSalary(), DELTA);
        assertEquals(3.54, c.getSkill(), DELTA);
    }

    @Test
    public void shouldIncreaseSalaryOnLevelUp() {
        float salary = testCook.getSalary();
        testCook.levelUp();

        assertTrue("Salary greater after lvl up", salary < testCook.getSalary());
    }
}
