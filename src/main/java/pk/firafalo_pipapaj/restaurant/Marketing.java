package pk.firafalo_pipapaj.restaurant;

public class Marketing {
	private String name;
	private float baseCost;
	private float fameRate;
	private float maxFame;
	private boolean isActive = false;

	public void setActive() {
		isActive = true;
	}

	public void setInActive() {
		isActive = false;
	}

	public boolean isActive() {
		return isActive;
	}

	public String getName() {
		return name;
	}

	public float getBaseCost() {
		return baseCost;
	}

	public float getFameRate() {
		return fameRate;
	}

	public float getMaxFame() {
		return maxFame;
	}

	public Marketing(String name, float baseCost, float fameRate, float maxFame) {
		this.name = name;
    	this.baseCost = baseCost;
    	this.fameRate = fameRate;
		this.maxFame = maxFame;
    }

	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder("");

		ret.append(this.displayPretty());
		ret.append("\nActive: ");
		ret.append(isActive());

		return ret.toString();
	}

	public String displayPretty() {
		StringBuilder ret = new StringBuilder("");

		ret.append(name);
		ret.append(", Cost: ");
		ret.append(baseCost);
		ret.append("\nIncrease in fame: ");
		ret.append(fameRate);
		ret.append(", Maximum fame: ");
		ret.append(maxFame);

		return ret.toString();
	}
}
