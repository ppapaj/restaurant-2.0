package pk.firafalo_pipapaj.restaurant;

public class Place {
    private String name;
    private String imageFilename;
    private float rent;
    private float attractiveness;
    private float fame;
    private int[] tables;
    private boolean isActive = false;

    public Place(String name, float rent, float attractiveness, float fame, int[] tables) {
    	this.name = name;
    	this.rent = rent;
    	this.attractiveness = attractiveness;
    	this.fame = fame;
        this.tables = tables;
    }

    public void setActive() {
        isActive = true;
    }

    public void setInActive() {
        isActive = false;
    }

    public boolean isActive() {
        return isActive;
    }

    public int getCapacity() {
        int sum = 0;
        for(int i = 0; i < tables.length; i++) {
            sum += tables[i]*(i+1);
        }
        return sum;
    }

    public String getImageFilename() {
        return imageFilename;
    }

    public int[] getTables() {
        return tables;
    }

    public void setTables(int[] tables) {
        this.tables = tables;
    }
    public float getFame() {
        return fame;
    }

    public float getRent() {
        return rent;
    }

    public float getAttractiveness() {
        return attractiveness;
    }

    public String getName() {
    	return name;
    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder("");

        ret.append(this.displayPretty());
        ret.append("\nActive: ");
        ret.append(isActive());

        return ret.toString();
    }

    public String displayPretty() {
        StringBuilder ret = new StringBuilder("");

        ret.append(name);
        ret.append(", Rent: ");
        ret.append(rent);
        ret.append("\nAttractiveness: ");
        ret.append(attractiveness);
        ret.append(", Fame: ");
        ret.append(fame);
        ret.append(", Capacity: ");
        ret.append(getCapacity());

        return ret.toString();
    }

    public void updateFame(Marketing m) {
        this.fame -= this.fame * 0.15;

        this.fame += this.fame * m.getFameRate();

        //ensure fame won't be higher than maxFame
        this.fame = Math.min(this.fame, m.getMaxFame());

        //ensure minimal fame is 0.1
        this.fame = Math.max(this.fame, 0.1f);

    }
}
