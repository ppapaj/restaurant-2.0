package pk.firafalo_pipapaj.restaurant.interfaces;

import pk.firafalo_pipapaj.restaurant.observers.ObservationType;

public interface Observer {
//    void update(String type);

    void update(ObservationType type);
    void update();
}