package pk.firafalo_pipapaj.restaurant.interfaces;

import pk.firafalo_pipapaj.restaurant.observers.ObservationType;

import java.util.ArrayList;

public interface Observable {

    ArrayList<Observer> observers = new ArrayList<>();

    void hook(Observer o);

    void removeAllObservers();

//    void notifyObservers(String type);
    void notifyObservers(ObservationType type);
}
