package pk.firafalo_pipapaj.restaurant.utils;

import pk.firafalo_pipapaj.restaurant.Marketing;
import pk.firafalo_pipapaj.restaurant.Place;
import pk.firafalo_pipapaj.restaurant.models.Cook;
import pk.firafalo_pipapaj.restaurant.models.Dish;
import pk.firafalo_pipapaj.restaurant.models.Waiter;

import java.util.List;

class SaveState {

    int week = 0, year = 0;
    float balance = 0.0f;

    String currentWaiterName = null;
    String currentPlaceName = null;
    String currentCookName = null;
    String currentMarketingName = null;

    List<Cook> cooks = null;
    List<Place> places = null;
    List<Waiter> waiters = null;
    List<Marketing> marketings = null;

    List<Dish> servedDishes = null;

}
