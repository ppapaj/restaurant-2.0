package pk.firafalo_pipapaj.restaurant.utils;

import javafx.stage.Stage;

public class StageManager {
    private static Stage primaryStage = null;

    private StageManager() {
        //avoid being instantiated
    }

    public static void setPrimaryStage(Stage s) {
        primaryStage = s;
    }

    public static Stage getPrimaryStage() {
        return primaryStage;
    }
}
