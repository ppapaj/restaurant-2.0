package pk.firafalo_pipapaj.restaurant.utils;

import com.google.gson.Gson;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import pk.firafalo_pipapaj.restaurant.Facade;
import pk.firafalo_pipapaj.restaurant.Marketing;
import pk.firafalo_pipapaj.restaurant.Place;
import pk.firafalo_pipapaj.restaurant.Turn;
import pk.firafalo_pipapaj.restaurant.models.Cook;
import pk.firafalo_pipapaj.restaurant.models.Waiter;

import java.io.*;

public class SaveHelper {
    public enum Slot {
        SLOT1,
        SLOT2,
        SLOT3
    }

    public static void saveGame(SaveHelper.Slot s) {
        SaveState ss = new SaveState();
        Facade f = Facade.getInstance();
        Turn t = f.getTurn();

        ss.week = t.getWeek();
        ss.year = t.getYear();
        ss.balance = t.getBalance();

        if(t.getCurrentCook() != null)
            ss.currentCookName = t.getCurrentCook().getName();
        if(t.getCurrentWaiter() != null)
            ss.currentWaiterName = t.getCurrentWaiter().getName();
        if(t.getCurrentPlace() != null)
            ss.currentPlaceName = t.getCurrentPlace().getName();
        if(t.getCurrentMarketing() != null)
            ss.currentMarketingName = t.getCurrentMarketing().getName();

        ss.cooks = f.getCooks();
        ss.places = f.getPlaces();
        ss.waiters = f.getWaiters();
        ss.marketings = f.getMarketing();

        ss.servedDishes = t.getServedDishes();


        Gson gson = new Gson();

        File dir = new File("./saves/");
        if(!dir.exists()) {
            dir.mkdir();
        }

        String filename = dir.getPath() + "/" + s.name().toLowerCase() + ".save";

        String json = gson.toJson(ss, SaveState.class);

        String encoded = Base64.encode(gson.toJson(ss, SaveState.class).getBytes());
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(filename))) {
            bw.write(encoded);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void loadGame(SaveHelper.Slot s) {

        String filename = "./saves/" + s.name().toLowerCase() + ".save";

        Gson gson = new Gson();

        String encoded = "";

        try(BufferedReader br = new BufferedReader(new FileReader(filename))) {
            while(br.ready())
                encoded += br.readLine();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        String json = new String(Base64.decode(encoded));

        SaveState ss = gson.fromJson(json, SaveState.class);

        Facade f = Facade.getInstance();
        Turn t = f.getTurn();

        t.setWeek(ss.week);
        t.setYear(ss.year);
        t.setBalance(ss.balance);
        t.setServedDishes(ss.servedDishes);

        f.setCooks(ss.cooks);
        f.setMarketings(ss.marketings);
        f.setPlaces(ss.places);
        f.setWaiters(ss.waiters);

        if(ss.currentCookName != null)
            t.selectCook(f.getCooks().stream()
                    .filter((Cook c) -> c.getName().equals(ss.currentCookName))
                    .findFirst().get());
        if(ss.currentWaiterName != null)
            t.selectWaiter(f.getWaiters().stream()
                    .filter((Waiter w) -> w.getName().equals(ss.currentWaiterName))
                    .findFirst().get());
        if(ss.currentPlaceName != null)
            t.selectPlace(f.getPlaces().stream()
                    .filter((Place p) -> p.getName().equals(ss.currentPlaceName))
                    .findFirst().get());
        if(ss.currentMarketingName != null)
            t.selectMarketing(f.getMarketing().stream()
                    .filter((Marketing m) -> m.getName().equals(ss.currentMarketingName))
                    .findFirst().get());

    }

    public static boolean saveExists(SaveHelper.Slot s) {
        File f = new File("./saves/" + s.name().toLowerCase() + ".save");

        return f.exists();
    }
}
