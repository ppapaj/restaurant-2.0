package pk.firafalo_pipapaj.restaurant.utils;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pk.firafalo_pipapaj.restaurant.controllers.GameController;
import pk.firafalo_pipapaj.restaurant.controllers.PopupController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Utils {
    public static ArrayList<String> getFileByLine(InputStream is) {
        ArrayList<String> ret = new ArrayList<>();
        String line;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(is))){
            while((line = reader.readLine()) != null) {
                if(line.startsWith("#"))
                    continue;
                ret.add(line);
            }
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        return ret;
    }

    public static void createWarning(String content) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning!");
        alert.setHeaderText(null);
        alert.setContentText(content);

        alert.showAndWait();
    }

    public static void createInformation(String content) {
        createInformation(content, null);
    }

    public static void createInformation(String content, String headerText) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText(headerText);
        alert.setContentText(content);

        alert.showAndWait();
    }

    public static void createPopup(String filename, String title) {
        FXMLLoader loader = new FXMLLoader(GameController.class.getClassLoader().getResource("fxml/" + filename));
        Parent root;
        try {
            root = loader.load();
            Stage stage = new Stage();
            stage.setTitle(title);
            stage.setScene(new Scene(root));
            stage.setResizable(false);
            stage.initModality(Modality.APPLICATION_MODAL);
            ((PopupController) loader.getController()).setStage(stage);
            stage.showAndWait();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
