package pk.firafalo_pipapaj.restaurant;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import pk.firafalo_pipapaj.restaurant.models.Cook;
import pk.firafalo_pipapaj.restaurant.models.Waiter;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class DataFactory {

    private static Gson gson = new Gson();
    private static JsonParser jsonParser = new JsonParser();

    public static List<Cook> createCookList() {
        List<Cook> cookList = new ArrayList<>();

        JsonArray arr = getJsonArrayFromFile("data/cooks.json");

        for(JsonElement e : arr) {
            cookList.add(gson.fromJson(e, Cook.class));
        }

        return cookList;
    }

    public static List<Waiter> createWaiterList() {
        List<Waiter> waiterList = new ArrayList<>();

        JsonArray arr = getJsonArrayFromFile("data/waiters.json");

        for(JsonElement e : arr) {
            waiterList.add(gson.fromJson(e, Waiter.class));
        }

        return waiterList;
    }

    public static List<Place> createPlaceList() {
        List<Place> placeList = new ArrayList<>();

        JsonArray arr = getJsonArrayFromFile("data/places.json");

        for(JsonElement e : arr) {
            placeList.add(gson.fromJson(e, Place.class));
        }

        return placeList;
    }

    public static List<Marketing> createMarketingList() {
        List<Marketing> marketingList = new ArrayList<>();

        JsonArray arr = getJsonArrayFromFile("data/marketing.json");

        for(JsonElement e : arr) {
            marketingList.add(gson.fromJson(e, Marketing.class));
        }

        return marketingList;
    }

    private static JsonArray getJsonArrayFromFile(String filename) {
        InputStreamReader is = new InputStreamReader(DataFactory.class.getClassLoader().getResourceAsStream(filename));

        return jsonParser.parse(is).getAsJsonArray();
    }

    public static List<Training> createTrainingList() {
        List<Training> trainingLst = new ArrayList<>();
        float price, rate;
        for (int i = 0; i < 3; i++) {
            price = (float) Math.random() * 100;
            rate = (float) Math.random() * 100;
            trainingLst.add(new Training(price, rate));
        }
        return trainingLst;
    }
}
