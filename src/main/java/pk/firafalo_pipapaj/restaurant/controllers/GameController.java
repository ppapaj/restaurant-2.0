package pk.firafalo_pipapaj.restaurant.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import pk.firafalo_pipapaj.restaurant.Facade;
import pk.firafalo_pipapaj.restaurant.Turn;
import pk.firafalo_pipapaj.restaurant.utils.SaveHelper;
import pk.firafalo_pipapaj.restaurant.utils.StageManager;
import pk.firafalo_pipapaj.restaurant.utils.Utils;


public class GameController {

    @FXML
    private ImageView cookImage;

    @FXML
    private ImageView waiterImage;

    @FXML
    private ImageView placeImage;

    @FXML
    private ImageView marketingImage;

    @FXML
    private Label weekLabel;

    @FXML
    private Label yearLabel;

    @FXML
    private Label budgetLabel;

    @FXML
    private Label cookText;

    @FXML
    private Label waiterText;

    @FXML
    private Label placeText;

    @FXML
    private Label marketingText;

    private Facade facade;

    @FXML
    private void initialize() {
        facade = Facade.getInstance();
        updateLabels();
    }

    public void changeCook(ActionEvent event) {
        Utils.createPopup("changeCook.fxml", "Change cook");
        updateLabels();
    }

    public void changeWaiter(ActionEvent event) {
        Utils.createPopup("changeWaiter.fxml", "Change waiter");
        updateLabels();
    }

    public void changePlace(ActionEvent event) {
        Utils.createPopup("changePlace.fxml", "Change place");
        updateLabels();
    }

    public void changeMarketing(ActionEvent event) {
        Utils.createPopup("changeMarketing.fxml", "Change marketing");
        updateLabels();
    }

    public void trainCook(ActionEvent event) {
        Utils.createPopup("trainCook.fxml", "Train cook");
        updateLabels();
    }

    public void trainWaiter(ActionEvent event) {
        Utils.createPopup("trainWaiter.fxml", "Train waiter");
        updateLabels();
    }

    public void next(ActionEvent event) {
        Turn turn = facade.getTurn();
        if (turn.getCurrentCook() == null) {
            Utils.createWarning("You have to select a cook before going to next week!");
        }
        else if (turn.getCurrentWaiter() == null) {
            Utils.createWarning("You have to select a waiter before going to next week!");
        }
        else if (turn.getCurrentPlace() == null) {
            Utils.createWarning("You have to select a place before going to next week!");
        }
        else if (turn.getCurrentMarketing() == null) {
            Utils.createWarning("You have to select a marketing before going to next week!");
        }
        else {
            turn.next();
            updateLabels();
            int weeksInRed = turn.getWeeksInRed();
            if (weeksInRed > 0) {
                if (weeksInRed == 3) {
                    Utils.createInformation("You're bankrupt!", "GAME OVER");
                    StageManager.getPrimaryStage().close();
                }
                else if (weeksInRed == 2) {
                    Utils.createInformation("You're 1 week from bankruptcy!");
                }
                else if (weeksInRed == 1) {
                    Utils.createInformation("You're 2 weeks from bankruptcy!");
                }
            }
        }
    }

    @FXML
    private void saveSlot1() {
        SaveHelper.saveGame(SaveHelper.Slot.SLOT1);
    }

    @FXML
    private void saveSlot2() {
        SaveHelper.saveGame(SaveHelper.Slot.SLOT2);
    }

    @FXML
    private void saveSlot3() {
        SaveHelper.saveGame(SaveHelper.Slot.SLOT3);
    }

    public void exit(ActionEvent event) {
        StageManager.getPrimaryStage().close();
    }

    private void updateLabels() {
        Turn turn = facade.getTurn();
        weekLabel.setText("Week: " + turn.getWeek());
        yearLabel.setText("Year: " + turn.getYear());
//        budgetLabel.setText("Budget: " + turn.getBalance());
        budgetLabel.setText(String.format("Budget: %1$.2f", turn.getBalance()));
        if(turn.getCurrentCook() != null)
            cookText.setText(turn.getCurrentCook().displayPretty());
        else {
            cookText.setText("This is a cook of yours");
        }
        if(turn.getCurrentWaiter() != null)
            waiterText.setText(turn.getCurrentWaiter().displayPretty());
        else {
            waiterText.setText("This is a waiter of yours");
        }
        if(turn.getCurrentPlace() != null)
            placeText.setText(turn.getCurrentPlace().displayPretty());
        else {
            placeText.setText("This is a place of yours");
        }
        if(turn.getCurrentMarketing() != null)
            marketingText.setText(turn.getCurrentMarketing().displayPretty());
        else {
            marketingText.setText("This is a marketing of yours");
        }
        cookImage.setImage(facade.getCookImage());
        waiterImage.setImage(facade.getWaiterImage());
        //TODO
        //placeImage.setImage(facade.getPlaceImage());
        //marketingImage.setImage(facade.getMarketingImage());
    }
}
