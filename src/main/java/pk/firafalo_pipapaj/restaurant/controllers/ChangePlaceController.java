package pk.firafalo_pipapaj.restaurant.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import pk.firafalo_pipapaj.restaurant.Facade;
import pk.firafalo_pipapaj.restaurant.Place;
import pk.firafalo_pipapaj.restaurant.Turn;
import pk.firafalo_pipapaj.restaurant.utils.Utils;

import java.util.List;
import java.util.stream.Collectors;

public class ChangePlaceController extends PopupController {
    @FXML
    ListView<Place> places;

    ObservableList<Place> items;

    @FXML
    private void initialize() {
        items = FXCollections.observableArrayList();
        facade = Facade.getInstance();
        items.addAll(facade.getPlaces());
        places.setItems(items);
    }

    public void ok(ActionEvent event) {
        Place selectedPlace  = places.getSelectionModel().getSelectedItem();
        if (selectedPlace != null && selectedPlace != facade.getTurn().getCurrentPlace() && selectedPlace.isActive() == true){
            facade.getTurn().selectPlace(selectedPlace);
            stage.close();
        }
        else if (selectedPlace != null && selectedPlace.isActive() == false) {
            Utils.createWarning("You have to buy a place before selecting!");
        }
        else {
            stage.close();
        }
    }

    public void buy(ActionEvent event) {
        Place selectedPlace = places.getSelectionModel().getSelectedItem();
        if (selectedPlace != null && selectedPlace.isActive() == false) {
            if (selectedPlace.getRent() > facade.getTurn().getBalance()) {
                Utils.createWarning("You don't have enough money!");
            }
            else {
                selectedPlace.setActive();
                places.getItems().clear();
                items.addAll(facade.getPlaces());
                places.setItems(items);
                facade.getTurn().reduceBalance(selectedPlace.getRent());
            }
        }
        else if (selectedPlace != null && selectedPlace.isActive() == true) {
            Utils.createWarning("You have already bought this place!");
        }
    }

    public void sell(ActionEvent event) {
        Place selectedPlace = places.getSelectionModel().getSelectedItem();
        if (selectedPlace != null && selectedPlace.isActive() == true) {
            List<Place> activePlaces = facade.getPlaces().stream()
                    .filter(place -> place.isActive() == true)
                    .collect(Collectors.toList());
            if (activePlaces.size() == 1) {
                Utils.createWarning("You can't sell the only place you have!");
            }
            else {
                selectedPlace.setInActive();
                places.getItems().clear();
                items.addAll(facade.getPlaces());
                places.setItems(items);
                facade.getTurn().increaseBalance(selectedPlace.getRent());
                if (selectedPlace == facade.getTurn().getCurrentPlace()) {
                    facade.getTurn().selectPlace(null);
                }
            }
        }
        else if (selectedPlace != null && selectedPlace.isActive() == false) {
            Utils.createWarning("You haven't bought this place!");
        }
    }
}
