package pk.firafalo_pipapaj.restaurant.controllers;

import pk.firafalo_pipapaj.restaurant.utils.Utils;

public class TrainCookController extends TrainEmployeeController {
    @Override
    public void train() {
        selectedTraining.train(facade.getTurn().getCurrentCook(), facade.getTurn());
        stage.close();
    }

    @Override
    public boolean isEmployeeSelected() {
        if (facade.getTurn().getCurrentCook() == null) {
            Utils.createWarning("You have to select your cook firstly!");
            return false;
        }
        return true;
    }
}
