package pk.firafalo_pipapaj.restaurant.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import pk.firafalo_pipapaj.restaurant.Facade;
import pk.firafalo_pipapaj.restaurant.Turn;
import pk.firafalo_pipapaj.restaurant.models.Waiter;
import pk.firafalo_pipapaj.restaurant.observers.LevelObserver;
import pk.firafalo_pipapaj.restaurant.utils.Utils;

import java.util.List;
import java.util.stream.Collectors;

public class ChangeWaiterController extends PopupController {
    @FXML
    ListView<Waiter> waiters;

    ObservableList<Waiter> items;

    @FXML
    private void initialize() {
        items = FXCollections.observableArrayList();
        facade = Facade.getInstance();
        items.addAll(facade.getWaiters());
        waiters.setItems(items);
    }

    public void ok(ActionEvent event) {
        Waiter selectedWaiter = waiters.getSelectionModel().getSelectedItem();
        if (selectedWaiter != null && selectedWaiter != facade.getTurn().getCurrentWaiter() && selectedWaiter.isActive() == true){
            facade.getTurn().selectWaiter(selectedWaiter);
            stage.close();
        }
        else if (selectedWaiter != null && selectedWaiter.isActive() == false) {
            Utils.createWarning("You have to buy a waiter before selecting!");
        }
        else {
            stage.close();
        }
    }

    public void buy(ActionEvent event) {
        Waiter selectedWaiter = waiters.getSelectionModel().getSelectedItem();
        if (selectedWaiter != null && selectedWaiter.isActive() == false) {
            if (selectedWaiter.getSalary() > facade.getTurn().getBalance()) {
                Utils.createWarning("You don't have enough money!");
            }
            else {
                selectedWaiter.setActive();
                waiters.getItems().clear();
                items.addAll(facade.getWaiters());
                waiters.setItems(items);
                facade.getTurn().reduceBalance(selectedWaiter.getSalary());
            }
        }
        else if (selectedWaiter != null && selectedWaiter.isActive() == true) {
            Utils.createWarning("You have already bought this waiter!");
        }
    }

    public void sell(ActionEvent event) {
        Waiter selectedWaiter = waiters.getSelectionModel().getSelectedItem();
        if (selectedWaiter != null && selectedWaiter.isActive() == true) {
            List<Waiter> activeWaiters = facade.getWaiters().stream()
                    .filter(waiter -> waiter.isActive() == true)
                    .collect(Collectors.toList());
            if (activeWaiters.size() == 1) {
                Utils.createWarning("You can't sell the only waiter you have!");
            }
            else {
                selectedWaiter.setInActive();
                waiters.getItems().clear();
                items.addAll(facade.getWaiters());
                waiters.setItems(items);
                facade.getTurn().increaseBalance(selectedWaiter.getSalary());
                if (selectedWaiter == facade.getTurn().getCurrentWaiter()) {
                    facade.getTurn().selectWaiter(null);
                }
            }
        }
        else if (selectedWaiter != null && selectedWaiter.isActive() == false) {
            Utils.createWarning("You haven't bought this waiter!");
        }
    }
}
