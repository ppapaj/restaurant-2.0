package pk.firafalo_pipapaj.restaurant.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import pk.firafalo_pipapaj.restaurant.Restaurant;
import pk.firafalo_pipapaj.restaurant.utils.SaveHelper;
import pk.firafalo_pipapaj.restaurant.utils.StageManager;

public class LoadGameController {

    @FXML
    private Button slot1Btn;

    @FXML
    private Button slot2Btn;

    @FXML
    private Button slot3Btn;

    @FXML
    private void initialize() {
        if(!SaveHelper.saveExists(SaveHelper.Slot.SLOT1))
            slot1Btn.setDisable(true);
        if(!SaveHelper.saveExists(SaveHelper.Slot.SLOT2))
            slot2Btn.setDisable(true);
        if(!SaveHelper.saveExists(SaveHelper.Slot.SLOT3))
            slot3Btn.setDisable(true);
    }

    public void slot1(ActionEvent event) {
        SaveHelper.loadGame(SaveHelper.Slot.SLOT1);
        startGame();
    }

    public void slot2(ActionEvent event) {
        SaveHelper.loadGame(SaveHelper.Slot.SLOT2);
        startGame();
    }

    public void slot3(ActionEvent event) {
        SaveHelper.loadGame(SaveHelper.Slot.SLOT3);
        startGame();
    }

    public void back(ActionEvent event) throws Exception {
        FXMLLoader loader = new FXMLLoader(Restaurant.class.getClassLoader().getResource("fxml/intro.fxml"));
        Parent root = loader.load();

        StageManager.getPrimaryStage().setScene(new Scene(root));
        StageManager.getPrimaryStage().show();
    }

    private void startGame() {
        FXMLLoader loader = new FXMLLoader(Restaurant.class.getClassLoader().getResource("fxml/game.fxml"));
        Parent root = null;
        try {
            root = loader.load();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        StageManager.getPrimaryStage().setScene(new Scene(root));
        StageManager.getPrimaryStage().show();
    }
}
