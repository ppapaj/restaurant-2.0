package pk.firafalo_pipapaj.restaurant.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import pk.firafalo_pipapaj.restaurant.Facade;
import pk.firafalo_pipapaj.restaurant.Turn;
import pk.firafalo_pipapaj.restaurant.models.Cook;
import pk.firafalo_pipapaj.restaurant.observers.CookObserver;
import pk.firafalo_pipapaj.restaurant.observers.LevelObserver;
import pk.firafalo_pipapaj.restaurant.utils.Utils;

import javax.swing.*;
import java.util.List;
import java.util.stream.Collectors;

public class ChangeCookController extends PopupController {
    @FXML
    ListView<Cook> cooks;

    ObservableList<Cook> items;

    @FXML
    private void initialize() {
        items = FXCollections.observableArrayList();
        facade = Facade.getInstance();
        items.addAll(facade.getCooks());
        cooks.setItems(items);
    }

    public void ok(ActionEvent event) {
        Cook selectedCook = cooks.getSelectionModel().getSelectedItem();
        if (selectedCook != null && selectedCook != facade.getTurn().getCurrentCook() && selectedCook.isActive() == true){
            facade.getTurn().selectCook(selectedCook);
            stage.close();
        }
        else if (selectedCook != null && selectedCook.isActive() == false) {
            Utils.createWarning("You have to buy a cook before selecting!");
        }
        else {
            stage.close();
        }
    }

    public void buy(ActionEvent event) {
        Cook selectedCook = cooks.getSelectionModel().getSelectedItem();
        if (selectedCook != null && selectedCook.isActive() == false) {
            if (selectedCook.getSalary() > facade.getTurn().getBalance()) {
                Utils.createWarning("You don't have enough money!");
            }
            else {
                selectedCook.setActive();
                cooks.getItems().clear();
                items.addAll(facade.getCooks());
                cooks.setItems(items);
                facade.getTurn().reduceBalance(selectedCook.getSalary());
            }
        }
        else if (selectedCook != null && selectedCook.isActive() == true) {
            Utils.createWarning("You have already bought this cook!");
        }
    }

    public void sell(ActionEvent event) {
        Cook selectedCook = cooks.getSelectionModel().getSelectedItem();
        if (selectedCook != null && selectedCook.isActive() == true) {
            List<Cook> activeCooks = facade.getCooks().stream()
                    .filter(cook -> cook.isActive() == true)
                    .collect(Collectors.toList());
            if (activeCooks.size() == 1) {
                Utils.createWarning("You can't sell the only cook you have!");
            }
            else {
                selectedCook.setInActive();
                cooks.getItems().clear();
                items.addAll(facade.getCooks());
                cooks.setItems(items);
                facade.getTurn().increaseBalance(selectedCook.getSalary());
                if (selectedCook == facade.getTurn().getCurrentCook()) {
                    facade.getTurn().selectCook(null);
                }
            }
        }
        else if (selectedCook != null && selectedCook.isActive() == false) {
            Utils.createWarning("You haven't bought this cook!");
        }
    }
}
