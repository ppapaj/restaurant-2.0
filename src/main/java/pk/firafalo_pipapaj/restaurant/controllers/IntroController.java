package pk.firafalo_pipapaj.restaurant.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import pk.firafalo_pipapaj.restaurant.Restaurant;
import pk.firafalo_pipapaj.restaurant.utils.StageManager;

public class IntroController {

    @FXML
    private void initialize() {
    }

    public void exit(ActionEvent event) {
        StageManager.getPrimaryStage().close();
    }

    public void newGame(ActionEvent event) throws Exception {
        FXMLLoader loader = new FXMLLoader(Restaurant.class.getClassLoader().getResource("fxml/game.fxml"));
        Parent root = loader.load();

        StageManager.getPrimaryStage().setScene(new Scene(root));
        StageManager.getPrimaryStage().show();
    }

    public void loadGame(ActionEvent event) throws Exception {
        FXMLLoader loader = new FXMLLoader(Restaurant.class.getClassLoader().getResource("fxml/loadGame.fxml"));
        Parent root = loader.load();

        StageManager.getPrimaryStage().setScene(new Scene(root));
        StageManager.getPrimaryStage().show();
    }
}
