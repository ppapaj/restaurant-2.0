package pk.firafalo_pipapaj.restaurant.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import pk.firafalo_pipapaj.restaurant.Facade;
import pk.firafalo_pipapaj.restaurant.Training;
import pk.firafalo_pipapaj.restaurant.utils.Utils;

public abstract class TrainEmployeeController extends PopupController{
    @FXML
    ListView<Training> trainings;

    public void init() {
        ObservableList<Training> items = FXCollections.observableArrayList();
        facade = Facade.getInstance();
        items.addAll(facade.getTurn().getTrainingList());
        trainings.setItems(items);
    }

    @FXML
    private void initialize() {
        init();
    }

    public Training selectedTraining;

    public void ok(ActionEvent event) {
        selectedTraining = trainings.getSelectionModel().getSelectedItem();
        if (selectedTraining == null || !isEmployeeSelected()) {
            stage.close();
        }
        else if (selectedTraining.getPrice() > facade.getTurn().getBalance()) {
            Utils.createWarning("You don't have enough money!");
        }
        else {
            train();
        }
    }

    public void train() {
        //must be overridden
    }

    public boolean isEmployeeSelected() {
        //must be overridden
        return true;
    }

    Label balance;

    public void setBalance(Label balance) {
        this.balance = balance;
    }
}
