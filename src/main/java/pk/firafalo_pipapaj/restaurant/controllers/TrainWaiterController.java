package pk.firafalo_pipapaj.restaurant.controllers;

import pk.firafalo_pipapaj.restaurant.utils.Utils;

public class TrainWaiterController extends TrainEmployeeController {
    @Override
    public void train() {
        selectedTraining.train(facade.getTurn().getCurrentWaiter(), facade.getTurn());
        stage.close();
    }

    @Override
    public boolean isEmployeeSelected() {
        if (facade.getTurn().getCurrentWaiter() == null) {
            Utils.createWarning("You have to select your waiter firstly!");
            return false;
        }
        return true;
    }
}
