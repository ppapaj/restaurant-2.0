package pk.firafalo_pipapaj.restaurant.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import pk.firafalo_pipapaj.restaurant.Facade;
import pk.firafalo_pipapaj.restaurant.Marketing;
import pk.firafalo_pipapaj.restaurant.Turn;
import pk.firafalo_pipapaj.restaurant.utils.Utils;

import java.util.List;
import java.util.stream.Collectors;

public class ChangeMarketingController extends PopupController {
    @FXML
    ListView<Marketing> marketings;

    ObservableList<Marketing> items;

    @FXML
    private void initialize() {
        items = FXCollections.observableArrayList();
        facade = Facade.getInstance();
        items.addAll(facade.getMarketing());
        marketings.setItems(items);
    }

    public void ok(ActionEvent event) {
        Marketing selectedMarketing  = marketings.getSelectionModel().getSelectedItem();
        if (selectedMarketing != null && selectedMarketing != facade.getTurn().getCurrentMarketing() && selectedMarketing.isActive() == true){
            facade.getTurn().selectMarketing(selectedMarketing);
            stage.close();
        }
        else if (selectedMarketing != null && selectedMarketing.isActive() == false) {
            Utils.createWarning("You have to buy a marketing before selecting!");
        }
        else {
            stage.close();
        }
    }

    public void buy(ActionEvent event) {
        Marketing selectedMarketing = marketings.getSelectionModel().getSelectedItem();
        if (selectedMarketing != null && selectedMarketing.isActive() == false) {
            if (selectedMarketing.getBaseCost() > facade.getTurn().getBalance()) {
                Utils.createWarning("You don't have enough money!");
            }
            else {
                selectedMarketing.setActive();
                marketings.getItems().clear();
                items.addAll(facade.getMarketing());
                marketings.setItems(items);
                facade.getTurn().reduceBalance(selectedMarketing.getBaseCost());
            }
        }
        else if (selectedMarketing != null && selectedMarketing.isActive() == true) {
            Utils.createWarning("You have already bought this marketing!");
        }
    }

    public void sell(ActionEvent event) {
        Marketing selectedMarketing = marketings.getSelectionModel().getSelectedItem();
        if (selectedMarketing != null && selectedMarketing.isActive() == true) {
            List<Marketing> activeMarketing = facade.getMarketing().stream()
                    .filter(marketing -> marketing.isActive() == true)
                    .collect(Collectors.toList());
            if (activeMarketing.size() == 1) {
                Utils.createWarning("You can't sell the only marketing you have!");
            }
            else {
                selectedMarketing.setInActive();
                marketings.getItems().clear();
                items.addAll(facade.getMarketing());
                marketings.setItems(items);
                facade.getTurn().increaseBalance(selectedMarketing.getBaseCost());
                if (selectedMarketing == facade.getTurn().getCurrentMarketing()) {
                    facade.getTurn().selectMarketing(null);
                }
            }
        }
        else if (selectedMarketing != null && selectedMarketing.isActive() == false) {
            Utils.createWarning("You haven't bought this marketing!");
        }
    }
}
