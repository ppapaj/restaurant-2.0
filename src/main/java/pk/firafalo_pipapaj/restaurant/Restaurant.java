package pk.firafalo_pipapaj.restaurant;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import pk.firafalo_pipapaj.restaurant.utils.StageManager;

public class Restaurant extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(Restaurant.class.getClassLoader().getResource("fxml/intro.fxml"));
        Parent root = loader.load();

        StageManager.setPrimaryStage(primaryStage);

        primaryStage.setTitle("Restaurant Tycoon");
        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    @Override
    public void stop(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Goodbye message");
        alert.setHeaderText(null);
        alert.setContentText("Thank you for playing!");

        alert.showAndWait();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
