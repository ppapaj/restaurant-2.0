package pk.firafalo_pipapaj.restaurant.models;

import pk.firafalo_pipapaj.restaurant.interfaces.Observer;
import pk.firafalo_pipapaj.restaurant.observers.ObservationType;

public class Cook extends Employee {
    public Cook(float salary, float skill, int level, String name) {
        this.setSalary(salary);
        this.setLevel(level);
        this.setName(name);
        this.setSkill(skill);
        this.setExperience(0);
    }

    @Override
    public void levelUp() {
        increaseLevel();
        increaseSalary(10.0f);
        //skill += 1;
    }

    public void increaseSkill(float rate) {
        float currentskill = getSkill();
        setSkill(currentskill * (1.0f + rate));
        notifyObservers(ObservationType.SKILLUP);
    }

    @Override
    public void notifyObservers(ObservationType type) {
        if (type.equals(ObservationType.SKILLUP)) {
            for(Observer o : observers) {
                o.update(ObservationType.SKILLUP);
            }
        }
        else if (type.equals(ObservationType.LEVELUP)) {
            for(Observer o : observers) {
                o.update(ObservationType.LEVELUP);
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder("");

        //ret.append("Cook ");
        ret.append(this.displayPretty());
        ret.append("\nActive: ");
        ret.append(isActive());

        return ret.toString();
    }

    public String displayPretty() {
        StringBuilder ret = new StringBuilder("");

        //ret.append("Cook ");
        ret.append(this.getName());
        ret.append(", Salary: ");
        ret.append(getSalary());
        ret.append(", Level: ");
        ret.append(getLevel());
        ret.append("\nSkill: ");
        ret.append(getSkill());

        return ret.toString();
    }
}
