package pk.firafalo_pipapaj.restaurant.models;

public class Dish {
    public enum Type {
        SOUP,
        MAIN_COURSE,
        DESSERT,
        INVALID;

        private static Type fromString(String s) {
            Type type;
            try {
                type = Type.valueOf(s.toUpperCase());
            } catch (IllegalArgumentException e) {
                System.err.println("IllegalArgumentException, setting type to INVALID.");
                type = Type.INVALID;
            }
            return type;
        }
    }

    private float price;
    private float cost;
    private float fanciness;
    private float difficulty, attractiveness;
    private Type type;

    public Dish(Type t, int difficulty, int attractiveness) {
        this.type = t;

        this.difficulty = difficulty;
        this.attractiveness = attractiveness;


        updateFanciness();
        updateCostAndPrice();
    }

    public Type getType() {
        return type;
    }

    public float getCost() {
        return cost;
    }

    public float getPrice() {
        return price;
    }

    private void updateCostAndPrice() {
        switch(type) {
            case SOUP:
                cost = 0.3f * fanciness;
                break;
            case MAIN_COURSE:
                cost = 0.5f * fanciness;
                break;
            case DESSERT:
                cost = 0.2f * fanciness;
                break;
            case INVALID:
                break;
        }

        price = 2.7f * cost;
    }

    private void updateFanciness() {
        fanciness = (difficulty + attractiveness) * 1.5f;
    }
    public void setDifficulty(float difficulty) {
        this.difficulty = difficulty;
        updateFanciness();
        updateCostAndPrice();
    }

    public void setAttractiveness(float attractiveness) {
        this.attractiveness = attractiveness;
        updateFanciness();
        updateCostAndPrice();
    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder("");

        switch(type) {
            case SOUP:
                ret.append("Soup:\n");
                break;
            case MAIN_COURSE:
                ret.append("Main course:\n");
                break;
            case DESSERT:
                ret.append("Dessert:\n");
                break;
            case INVALID:
                return "INVALID DISH";
        }
        ret.append("; Cost: ");
        ret.append(cost);
        ret.append("Price: ");
        ret.append(price);
        ret.append('\n');

        return ret.toString();
    }
}
