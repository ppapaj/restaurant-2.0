package pk.firafalo_pipapaj.restaurant.models;

public class Waiter extends Employee {
    private float appearance;

    public Waiter(float salary, float skill, float appearance, String name, int level) {
        this.setSalary(salary);
        this.appearance = appearance;
        this.setName(name);
        this.setLevel(level);
        this.setSkill(skill);
        this.setExperience(0);
    }

    public int getMaxAppearance() {
        return getLevel()*2;
    }

    public void setAppearance(int appearance) {
        if(appearance > getMaxAppearance())
            this.appearance = getMaxAppearance();
        else
            this.appearance = appearance;
    }
    public float getAppearance() {
        return appearance;
    }

    public void increaseSkill(float rate) {
        float currentskill = getSkill();
        setSkill(currentskill * (1.0f + rate));
        appearance = appearance * (1.0f + rate);
    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder("");

        ret.append(this.displayPretty());
        ret.append("\nActive: ");
        ret.append(isActive());

        return ret.toString();
    }

    public String displayPretty() {
        StringBuilder ret = new StringBuilder("");

        ret.append(this.getName());
        ret.append(", Salary: ");
        ret.append(getSalary());
        ret.append(", Level: ");
        ret.append(getLevel());
        ret.append("\nSkill: ");
        ret.append(getSkill());
        ret.append(", Appearance: ");
        ret.append(getAppearance());

        return ret.toString();
    }
}
