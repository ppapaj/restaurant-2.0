package pk.firafalo_pipapaj.restaurant.models;

import pk.firafalo_pipapaj.restaurant.interfaces.Observable;
import pk.firafalo_pipapaj.restaurant.interfaces.Observer;
import pk.firafalo_pipapaj.restaurant.observers.ObservationType;

import java.util.ArrayList;

public abstract class Employee implements Observable {
    private int level;
    private int experience;
    private float salary;
    private float skill;
    private boolean isActive = false;
    private String name;
    private String imageFilename;
    transient ArrayList<Observer> observers;

    public void setActive() {
        isActive = true;
    }

    public void setInActive() {
        isActive = false;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }

    public void increaseLevel() {
        level++;
    }

    public float getMaxSkill() {
        return level*2;
    }

    public void increaseSalary(float amount) {
        salary += amount;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public int getExperience() {
        return experience;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSkill(float skill) {
        if(skill > getMaxSkill())
            this.skill = getMaxSkill();
        else
            this.skill = skill;
    }

    public float getSkill() {
        return skill;
    }

    public void levelUp() {
        increaseLevel();
//        setSalary(2*level*level - 3*level + 7);
        increaseSalary(10.0f);
    }

    public void gainExperience(int experience) {
        this.experience += experience;
        while (this.experience >= 100) {
            this.experience -= 100;
            notifyObservers(ObservationType.LEVELUP);
        }
    }

    abstract public void increaseSkill(float rate);

    @Override
    public void notifyObservers(ObservationType type) {
        if (type.equals(ObservationType.LEVELUP)) {
            for(Observer o : observers) {
                o.update(ObservationType.LEVELUP);
            }
        }
    }

    @Override
    public void hook(Observer o) {
        if (observers == null) {
            observers = new ArrayList<>();
        }
        observers.add(o);
    }

    @Override
    public void removeAllObservers() {
        if (observers != null) {
            observers.clear();
        }
    }

    public String getImageFilename() {
        return imageFilename;
    }

    public void setImageFilename(String imageFilename) {
        this.imageFilename = imageFilename;
    }
}
