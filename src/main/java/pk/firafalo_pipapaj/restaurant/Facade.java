package pk.firafalo_pipapaj.restaurant;

import javafx.scene.image.Image;
import pk.firafalo_pipapaj.restaurant.models.Cook;
import pk.firafalo_pipapaj.restaurant.models.Waiter;
import pk.firafalo_pipapaj.restaurant.observers.TurnObserver;

import java.util.List;

public class Facade {

    private static Facade instance = null;

    private Facade() {
        //Just to avoid instantiation
    }

    public static Facade getInstance() {
        if(instance == null)
            instance = new Facade();
        return instance;
    }

    private Turn turn;
    private List<Marketing> marketings;
    private List<Place> places;
    private List<Cook> cooks;
    private List<Waiter> waiters;

    public List<Cook> getCooks() {
        if (cooks == null) {
            cooks = DataFactory.createCookList();
        }
        return cooks;
    }

    public List<Waiter> getWaiters() {
        if (waiters == null) {
            waiters = DataFactory.createWaiterList();
        }
        return waiters;
    }

    public List<Place> getPlaces() {
        if (places == null) {
            places = DataFactory.createPlaceList();
        }
        return places;
    }

    public List<Marketing> getMarketing() {
        if (marketings == null) {
            marketings = DataFactory.createMarketingList();
        }
        return marketings;
    }

    public Turn getTurn() {
        if (turn == null) {
            turn = new Turn();
            new TurnObserver(turn);
        }
        return turn;
    }

    public Image getMarketingImage() {
        return turn.getMarketingImage();
    }

    public Image getPlaceImage() {
        return turn.getPlaceImage();
    }

    public Image getWaiterImage() {
        return turn.getWaiterImage();
    }

    public Image getCookImage() {
        return turn.getCookImage();
    }

    public void setMarketings(List<Marketing> marketings) {
        this.marketings = marketings;
    }

    public void setPlaces(List<Place> places) {
        this.places = places;
    }

    public void setCooks(List<Cook> cooks) {
        this.cooks = cooks;
    }

    public void setWaiters(List<Waiter> waiters) {
        this.waiters = waiters;
    }
}
