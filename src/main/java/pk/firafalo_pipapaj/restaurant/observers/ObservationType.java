package pk.firafalo_pipapaj.restaurant.observers;

public enum ObservationType {
    LEVELUP,
    SKILLUP,
    NONE
}
