package pk.firafalo_pipapaj.restaurant.observers;

import pk.firafalo_pipapaj.restaurant.Turn;
import pk.firafalo_pipapaj.restaurant.interfaces.Observer;
import pk.firafalo_pipapaj.restaurant.models.Dish;

public class CookObserver implements Observer {

    Turn turn;

    public CookObserver(Turn turn) {
        this.turn = turn;
        this.turn.getCurrentCook().hook(this);
    }

    @Override
    public void update(ObservationType type) {
        if (!type.equals(ObservationType.SKILLUP))
            return;
        for (Dish d : turn.getServedDishes()) {
            //do something
            d.setDifficulty(turn.getCurrentCook().getSkill());
        }
    }

    @Override
    public void update() {
        // TODO Auto-generated method stub

    }

}