package pk.firafalo_pipapaj.restaurant.observers;

import pk.firafalo_pipapaj.restaurant.interfaces.Observer;
import pk.firafalo_pipapaj.restaurant.models.Employee;
import pk.firafalo_pipapaj.restaurant.utils.Utils;

public class LevelObserver implements Observer {
    Employee employee;

    public LevelObserver(Employee employee) {
        this.employee = employee;
        this.employee.hook(this);
    }

    @Override
    public void update(ObservationType type) {
        if (!type.equals(ObservationType.LEVELUP))
            return;
        float salaryChange = employee.getSalary();
        employee.levelUp();
        salaryChange = employee.getSalary() - salaryChange;
        employee.setExperience(employee.getExperience() - 100);

        String information = "Your employee " + employee.getName();
        information += " is now on level " + employee.getLevel();
        information += String.format(" and will now earn %1$1.2f more money!", salaryChange);

        Utils.createInformation(information, "LEVEL UP!");
    }

    @Override
    public void update() {
        // TODO Auto-generated method stub

    }

}