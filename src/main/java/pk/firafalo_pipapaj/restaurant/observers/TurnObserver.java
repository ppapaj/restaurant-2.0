package pk.firafalo_pipapaj.restaurant.observers;

import pk.firafalo_pipapaj.restaurant.Turn;
import pk.firafalo_pipapaj.restaurant.interfaces.Observer;
import pk.firafalo_pipapaj.restaurant.utils.Utils;

public class TurnObserver implements Observer {
    Turn turn;

    public TurnObserver(Turn turn) {
        this.turn = turn;
        this.turn.hook(this);
    }

    @Override
    public void update() {
        int week = turn.getWeek();
        turn.countWeekClients();
        turn.nextWeek();
        float balanceChange = turn.getBalance();
        turn.updateBudget();
        balanceChange = turn.getBalance() - balanceChange;
        float fameChange = turn.getCurrentPlace().getFame();
        turn.updateFame();
        fameChange = turn.getCurrentPlace().getFame() - fameChange;
        turn.updateEmployees();
        turn.modifyTrainingList();

        String information = "End of week " + week + ".\n";
        information += "You had " + turn.getNumberOfClients() + " clients.\n";
        information += balanceChange < 0 ? "You lost " : "You earned ";
        information += String.format("%1$.2f.\n", Math.abs(balanceChange));
        information += fameChange < 0 ? "Your fame has decreased by " : "Your fame has increased by ";
        information += String.format("%1$.3f\n", Math.abs(fameChange));

        Utils.createInformation(information);
    }

    @Override
    public void update(ObservationType type) {
        // TODO Auto-generated method stub

    }
}
