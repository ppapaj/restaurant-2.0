package pk.firafalo_pipapaj.restaurant;

import javafx.scene.image.Image;
import pk.firafalo_pipapaj.restaurant.interfaces.Observable;
import pk.firafalo_pipapaj.restaurant.interfaces.Observer;
import pk.firafalo_pipapaj.restaurant.models.Cook;
import pk.firafalo_pipapaj.restaurant.models.Dish;
import pk.firafalo_pipapaj.restaurant.models.Waiter;
import pk.firafalo_pipapaj.restaurant.observers.CookObserver;
import pk.firafalo_pipapaj.restaurant.observers.LevelObserver;
import pk.firafalo_pipapaj.restaurant.observers.ObservationType;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Turn implements Observable {
    private int week = 1, year = 2016;
    private float balance = 300.0f;
    private float income = 0.0f;
    private float expenses = 0.0f;
    private float salaries = 0.0f;
    private List<Dish> servedDishes;
    private List<Training> trainingList;
    private Place currentPlace = null;
    private Waiter currentWaiter = null;
    private Cook currentCook = null;
    private Marketing currentMarketing = null;

    private int soldSoups;
    private int soldMainCourses;
    private int soldDesserts;
    private int numberOfClients;

    private Image waiterImage = null;
    private Image cookImage = null;
    private Image placeImage = null;
    private Image marketingImage = null;

    private Random random = new Random();

    private int weeksInRed = 0;

    public Turn() {
        createDishes();
        trainingList = DataFactory.createTrainingList();
    }

    private void createDishes() {
        servedDishes = new ArrayList<>();
        servedDishes.add(new Dish(Dish.Type.MAIN_COURSE, 0, 0));
        servedDishes.add(new Dish(Dish.Type.SOUP, 0, 0));
        servedDishes.add(new Dish(Dish.Type.DESSERT, 0, 0));
    }

    @Override
    public void hook(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeAllObservers() {
        observers.clear();
    }

    @Override
    public void notifyObservers(ObservationType type) {
        observers.forEach(Observer::update);
    }

    public void next() {
        notifyObservers(ObservationType.NONE);
    }

    public void nextWeek() {
        week++;
        if (week % 52 == 0) {
            year++;
            week = 1;
        }
    }

    public void countWeekClients() {
        numberOfClients = Math.round((currentPlace.getFame() + 0.2f) * currentPlace.getCapacity());
        soldSoups = Math.round(0.6f * random.nextFloat() + 0.6f) * numberOfClients;
        soldMainCourses = Math.round(0.6f * random.nextFloat() + 0.6f) * numberOfClients;
        soldDesserts = Math.round(0.6f * random.nextFloat() + 0.6f) * numberOfClients;
    }

    public void updateBudget() {
        calculateExpenses();
        calculateIncome();
        balance += (income - expenses);
        if (balance < 0) {
            weeksInRed++;
        }
        else {
            weeksInRed = 0;
        }
    }

    public void updateFame() {
        if(currentMarketing == null)
            return;

        currentPlace.updateFame(currentMarketing);

    }

    private void calculateIncome() {
        income = 0;

        for(Dish d: servedDishes) {
            switch (d.getType()) {
                case SOUP:
                    income += soldSoups * d.getPrice();
                    break;
                case MAIN_COURSE:
                    income += soldMainCourses * d.getPrice();
                    break;
                case DESSERT:
                    income += soldDesserts * d.getPrice();
                    break;
                case INVALID:
                    break;
            }
        }

        income += income * currentWaiter.getAppearance() * 0.11f*random.nextFloat();

    }

    private void calculateExpenses() {
        expenses = 0;

        for(Dish d: servedDishes) {
            switch (d.getType()) {
                case SOUP:
                    expenses += soldSoups * d.getCost();
                    break;
                case MAIN_COURSE:
                    expenses += soldMainCourses * d.getCost();
                    break;
                case DESSERT:
                    expenses += soldDesserts * d.getCost();
                    break;
                case INVALID:
                    break;
            }
        }

        expenses += salaries;
    }

    public void updateEmployees() {
        currentCook.gainExperience(numberOfClients);
        currentWaiter.gainExperience(numberOfClients);
    }
    public void modifyTrainingList() {
        float price, rate;
        for (Training t : trainingList) {
            price = (float) Math.random() * 100;
            rate = (float) Math.random() * 100;
            t.setPrice(price);
            t.setBaseRate(rate);
        }
    }

    public void selectPlace(Place p) {
        if (currentPlace != null) {
            salaries -= currentPlace.getRent();
        }
        currentPlace = p;
        if (p != null) {
            salaries += currentPlace.getRent();
            //now dishes may change
            for (Dish d : servedDishes) {
                d.setAttractiveness(p.getAttractiveness());
            }
        }
    }

    public void selectCook(Cook c) {
        if (currentCook != null) {
            salaries -= currentCook.getSalary();
        }
        currentCook = c;
        cookImage = null;
        if (c != null) {
            salaries += currentCook.getSalary();
            cookImage = new Image(this.getClass().getResourceAsStream("/" + c.getImageFilename()));
            new LevelObserver(currentCook);
            new CookObserver(this);
//        now dishes may change
            for (Dish d : servedDishes) {
                d.setDifficulty(c.getSkill());
            }
        }
    }

    public void selectWaiter(Waiter w) {
        if (currentWaiter != null) {
            salaries -= currentWaiter.getSalary();
        }
        currentWaiter = w;
        waiterImage = null;
        if (w != null) {
            salaries += currentWaiter.getSalary();
            waiterImage = new Image(this.getClass().getResourceAsStream("/" + w.getImageFilename()));
            new LevelObserver(currentWaiter);
        }
    }

    public void selectMarketing(Marketing m) {
        if (currentMarketing != null) {
            salaries -= currentMarketing.getBaseCost();
        }
        currentMarketing = m;
        if (m != null) {
            salaries += currentMarketing.getBaseCost();
        }
    }

    public float getBalance() {
        return balance;
    }

    public int getWeek() {
        return week;
    }

    public int getYear() {
        return year;
    }

    public void reduceBalance(float price) {
        this.balance -= price;
    }

    public void increaseBalance(float price) {
        this.balance += price;
    }

    public Place getCurrentPlace() {
        return currentPlace;
    }

    public Waiter getCurrentWaiter() {
        return currentWaiter;
    }

    public Cook getCurrentCook() {
        return currentCook;
    }

    public Marketing getCurrentMarketing() {
        return currentMarketing;
    }

    public List<Dish> getServedDishes() {
        return servedDishes;
    }

    public void setServedDishes(List<Dish> servedDishes) {
        this.servedDishes = servedDishes;
    }

    public List<Training> getTrainingList() { return trainingList; }

    public int getNumberOfClients() {
        return numberOfClients;
    }

    public float getSalaries() {
        return salaries;
    }

    public Image getWaiterImage() {
        return waiterImage;
    }

    public Image getCookImage() {
        return cookImage;
    }

    public Image getPlaceImage() {
        return placeImage;
    }

    public Image getMarketingImage() {
        return marketingImage;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public int getWeeksInRed() {
        return weeksInRed;
    }
}
