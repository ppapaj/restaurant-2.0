package pk.firafalo_pipapaj.restaurant;

import pk.firafalo_pipapaj.restaurant.models.Employee;

public class Training {
	private float price;
	private float baseRate;
	
	public Training(float price, float baseRate) {
		this.price = price;
		this.baseRate = baseRate;
	}
	
	public float getPrice(){
		return price;
	}
	
	public void setPrice(float price) {
		this.price = price;
	}
		
	public float getBaseRate(){
		return baseRate;
	}
	
	public void setBaseRate(float baseRate){
		this.baseRate = baseRate;
	}

	public void train(Employee employee, Turn turn) {
		float rate;
		if (Math.random() > 0.5) {
			rate = baseRate * 0.85f;
		}
		else {
			rate = baseRate * 1.15f;
		}
		employee.increaseSkill(rate);
		turn.reduceBalance(price);
	}

	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder("");

		ret.append("Training\nPrice: ");
		ret.append(price);
		ret.append(", Rate: ");
		ret.append(baseRate);

		return ret.toString();
	}
}
